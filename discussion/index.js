// Function Parameters and Arguments

// "name" is called a parameter that acts as a named variable/container that exists only inside of the function.

// "Nehemiah" - argument - it is the information/data provided directly into the function.

function printInput(name) {
	console.log("My name is " + name);
};

printInput("Nehemiah")
// We can also add a printInput("") as many.
printInput("CJ");
printInput("Allan");


// Another example:
let sampleVariable = "Yui"
printInput(sampleVariable);

/*-----------------------------------------*/

// Example:
function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
	}

	checkDivisibilityBy8(64);
	checkDivisibilityBy8(28);


/*-----------------------------------------*/

// Functions as Arguments

// Function parameters can also accept other function as arguments.
/*function inside function na*/
//orange na argumentFunction is a Parameter.

// Example:
function argumentFunction(){
	console.log("This function was passed as an argument before the message was printed.")
}


function invokeFunction(argumentFunction){
	argumentFunction();

//console.log("This code comes from invokeFunction"); palatandaan, pwede rin ito na di na lagyan or e,type.
// array of objects sir gamit na gamit ata yan.
	console.log("This code comes from invokeFunction");
}

invokeFunction(argumentFunction);



/*---------------------------------------*/

// Multiple Arguments - will correspond to the number of "Parameters" declared in a function in succeeding order.

function createFullName(firstName, middleName, lastName) {
	console.log(firstName + ' ' + middleName + ' ' + lastName);
}

createFullName('Juan', 'Dela');
createFullName('Jane', 'Dela', 'Cruz');
createFullName('Jane', 'Dela', 'Cruz', 'Hello')

/* Another example:----------------
in here, gumamit tayo ng variable na "john"*/

let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

// Dito nag,ccall nlng tayo ng fistName,  middleName, lastName.
createFullName(firstName, middleName, lastName);


/*------------------------------------------*/

// The Return Statement - allows us to output a value from a function to be passed o the line/block of code that invoked/called the function.

// The 'return' statement also stops the execution of the function and any code after the return statement will not be executed.

function returnFullName(firstName, middleName, lastName) {



	return firstName + ' ' + middleName + ' ' + lastName;

	console.log("This message will not be printed.");
	
}

let completeName = returnFullName('Joe', 'Jayson', 'Helberg');
console.log(completeName)

/*-----------------------------------------*/



function returnAddress(city, country){
	let fullAddress = city + ", " + country;

	return fullAddress;

}

let myAddress = returnAddress("Cebu City", "Philippines");

console.log(myAddress);
/*------------------------------------------*/




function getAverage(num1, num2, num3) {

	let average = (num1 + num2 + num3) / 3;

	console.log(average);
	
}

function printWords(getAverage) {
	console.log("This is your Average");

	getAverage(10, 20, 30);

	console.log("Thank you!");
}

printWords(getAverage);